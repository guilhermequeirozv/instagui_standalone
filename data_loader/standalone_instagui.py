#!/usr/bin python3
import sys
import os
import xml.etree.ElementTree as ET
import time
import cx_Oracle
import paramiko

def handle_descriptor(descriptor, img_id, descriptor_name, tags):
	concat = []
	for i,tag in enumerate(tags):
		if descriptor.find(tag) is not None:
			elements = descriptor.find(tag).text.split()
			for element in elements:
				concat.append(int(element))
	return concat

def parse_line(line):
	line = line.strip()
	mydir = '/home/cohen/cophir/'+line
	root = ET.parse(mydir).getroot()
	photo = root.find('photo')

	# get username and id
	img_id = photo.get('id')
	owner = photo.find('owner')
	username = owner.items()[1][1]

	# get date
	dates = photo.find('dates')
	date_taken = dates.items()[1][1]

	#get tags
	tags = photo.find('tags')
	tag_array = tags.findall('tag')
	all_tags = []
	for tag in tag_array:
		all_tags.append(tag.text)

	#get url
	url = ''
	if photo.find('url') is not None:
		url = photo.find('url').text
	
	#get feature vectors
	mpeg = root.find('Mpeg7')
	multimedia = None
	for elem in mpeg.iter():
		if elem.tag == 'MultimediaContent':
			multimedia = elem
	image = multimedia.find('Image')
	feature_vetors = image.findall('VisualDescriptor')
	descriptors = {}
	for feature_vetor in feature_vetors:
		if feature_vetor.get('type') == 'ScalableColorType':
			descriptors['scalable'] = handle_descriptor(feature_vetor, img_id, 'scalable', ['Coeff'])
		elif feature_vetor.get('type') == 'ColorStructureType':
			descriptors['structure'] = handle_descriptor(feature_vetor, img_id, 'structure', ['Values'])
		elif feature_vetor.get('type') == 'ColorLayoutType':
			descriptors['layout'] = handle_descriptor(feature_vetor, img_id, 'layout',
				['YDCCoeff', 'CbDCCoeff', 'CrDCCoeff', 'YACCoeff5', 'CbACCoeff2', 'CrACCoeff2'])
		elif feature_vetor.get('type') == 'EdgeHistogramType':
			descriptors['histogram'] = handle_descriptor(feature_vetor, img_id, 'histogram', ['BinCounts'])
		elif feature_vetor.get('type') == 'HomogeneousTextureType':
			descriptors['texture'] = handle_descriptor(feature_vetor, img_id, 'texture',
				  ['Average', 'StandardDeviation', 'Energy', 'EnergyDeviation'])

	#build object
	image = { 'username_id':username, 'img_id': img_id, 'taken_date': date_taken, 'tags': all_tags, 'photo_link': url, 'descriptors': descriptors, 'xml_path': line}
	str_img = str(username)+';'+str(img_id)+';'+str(date_taken)+';'+str(url)+';'\
		  +str(descriptors['scalable']).replace('[','').replace(']','')+';'\
		  +str(descriptors['structure']).replace('[','').replace(']','')+';'\
		  +str(descriptors['layout']).replace('[','').replace(']','')+';'\
		  +str(descriptors['histogram']).replace('[','').replace(']','')+';'\
		  +str(descriptors['texture']).replace('[','').replace(']','')+';'
	print(str_img)
	return image

def insertIntoOracle(image):
	con_str='instagui_oper/instagoper@172.17.0.1/ORCLPDB1'
	con = cx_Oracle.connect(con_str, encoding="UTF-8", nencoding="UTF-8")
	sql = "insert into USERIMAGES_HISTORY(IMG_ID,XML_PATH,PHOTO_LINK,USERNAME_ID,TAKEN_DATE) "\
		  "values (:IMG_ID,:XML_PATH,:PHOTO_LINK,:USERNAME_ID,to_Date(:TAKEN_DATE,'YYYY-MM-DD HH24:MI:SS'))"
	cur = con.cursor()
	cur.execute(sql, image)
	cur.close()
	con.commit()

def loadFeatureVector(image):
	con_str='instagui_oper/instagoper@172.17.0.1/ORCLPDB1'
	con = cx_Oracle.connect(con_str, encoding="UTF-8", nencoding="UTF-8")
	sql = "BEGIN "\
		  "  FOR rec IN (SELECT img_id, scalable FROM userimages_history where img_id = '{0}' FOR UPDATE) LOOP "\
		  "  	DBMS_OUTPUT.PUT_LINE(rec.img_id || ': ' || readSignatureFromFile('/home/oracle/' || TO_CHAR(rec.img_id) || '.txt', rec.scalable, 64, 0));"\
		  "  END LOOP;"\
		  "END;".format(image['img_id'])
	cur = con.cursor()
	cur.execute(sql)
	cur.close()
	con.commit()

def create_file(image):
	file = open('/home/cohen/oracleSharedDir/'+image['img_id']+'.txt','w')
	for feature in image['descriptors']['scalable']:
		file.write(str(feature)+' ')
	file.close()

with open("/home/cohen/cophir/cophir_chunk.txt") as file:
	for line in file:
		image = parse_line(line)
		create_file(image)
		del image['descriptors']
		del image['tags']
		try:
			insertIntoOracle(image)
			loadFeatureVector(image)
			os.remove('/home/cohen/oracleSharedDir/'+image['img_id']+'.txt')
		except Exception as e:
			print(e)
			pass