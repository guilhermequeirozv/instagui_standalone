--with sysdba-privileged user

CREATE TABLESPACE INSTAGUSERS_ALL 
DATAFILE '/opt/oracle/oradata/ORCLCDB/ORCLPDB1/instagusers_all.dbf' 
SIZE 200M AUTOEXTEND ON NEXT 100M
EXTENT MANAGEMENT LOCAL
SEGMENT SPACE MANAGEMENT AUTO;

CREATE TABLESPACE INSTAGUSERS 
DATAFILE '/opt/oracle/oradata/ORCLCDB/ORCLPDB1/instagusers.dbf' 
SIZE 100M AUTOEXTEND ON NEXT 50M
EXTENT MANAGEMENT LOCAL
SEGMENT SPACE MANAGEMENT AUTO;

CREATE TABLESPACE INSTAGENERAL 
DATAFILE '/opt/oracle/oradata/ORCLCDB/ORCLPDB1/instageneral.dbf' 
SIZE 100M AUTOEXTEND ON NEXT 50M
EXTENT MANAGEMENT LOCAL
SEGMENT SPACE MANAGEMENT AUTO;

--creating operational user
create user instagui_oper identified by instagoper default tablespace instagusers quota unlimited on instagusers;
ALTER USER INSTAGUI_OPER QUOTA UNLIMITED ON INSTAGUSERS_ALL;
ALTER USER INSTAGUI_OPER QUOTA UNLIMITED ON INSTAGENERAL;
grant create session to INSTAGUI_OPER with admin option;
GRANT CREATE TABLE TO INSTAGUI_OPER;
grant CREATE TRIGGER to instagui_oper;
grant create public synonym to instagui_oper;

--after creating tables
grant ALL on INSTAGUI_OPER.USERIMAGES_HISTORY to instagui_oper;
grant ALL on INSTAGUI_OPER.USERPROFILE_HISTORY to instagui_oper;
grant ALL on INSTAGUI_OPER.USERPROFILE to instagui_oper;
grant ALL on INSTAGUI_OPER.USERIMAGES to instagui_oper;
grant ALL on INSTAGUI_OPER.COMMENTS to instagui_oper;
grant ALL on INSTAGUI_OPER.FOLLOWING to instagui_oper;
grant ALL on INSTAGUI_OPER.LIKEDCOMMENTS to instagui_oper;
grant ALL on INSTAGUI_OPER.LIKEDIMAGES to instagui_oper;
grant create user to instagui_oper;
grant alter user to instagui_oper;
grant drop user to instagui_oper;

--creating applicaton user
create user instagui_user identified by instaguser default tablespace INSTAGENERAL quota unlimited on INSTAGENERAL;
grant create session to instagui_user;
grant select,update on INSTAGUI_OPER.USERPROFILE to instagui_user;
grant select,update on INSTAGUI_OPER.USERIMAGES to instagui_user;
grant select,insert,update on INSTAGUI_OPER.COMMENTS to instagui_user;
grant select,insert,update on INSTAGUI_OPER.FOLLOWING to instagui_user;
grant select,insert,update on INSTAGUI_OPER.LIKEDCOMMENTS to instagui_user;
grant select,insert,update on INSTAGUI_OPER.LIKEDIMAGES to instagui_user;
GRANT CREATE SEQUENCE TO INSTAGUI_OPER;

create public synonym USERPROFILE for INSTAGUI_OPER.USERPROFILE;
create public synonym USERIMAGES for INSTAGUI_OPER.USERIMAGES;
create public synonym COMMENTS for INSTAGUI_OPER.COMMENTS;
create public synonym FOLLOWING for INSTAGUI_OPER.FOLLOWING;
create public synonym LIKEDCOMMENTS for INSTAGUI_OPER.LIKEDCOMMENTS;
create public synonym LIKEDIMAGES for INSTAGUI_OPER.LIKEDIMAGES;
grant drop public synonym to instagui_oper;
drop public synonym USERPROFILE_HISTORY;
drop public synonym USERIMAGES_HISTORY;

grant create library to instagui_oper;
grant create procedure to instagui_oper;
grant create operator to instagui_oper;
grant create type to instagui_oper;
grant create indextype to instagui_oper;