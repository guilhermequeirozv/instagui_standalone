DROP TRIGGER create_userprofile_history;
DROP TRIGGER create_userprofile;
DROP TRIGGER create_userimages;
DROP TABLE LIKEDIMAGES;
DROP TABLE LIKEDCOMMENTS;
DROP TABLE FOLLOWING;
DROP TABLE COMMENTS;
DROP TABLE USERIMAGES;
DROP TABLE USERPROFILE;
DROP TABLE USERIMAGES_HISTORY;
DROP TABLE USERPROFILE_HISTORY;

CREATE TABLE USERPROFILE_HISTORY
(   
    USERNAME_ID VARCHAR2(255), 
    IMG_ID VARCHAR2(13),
    ACTIVE NUMBER(1) DEFAULT 0,
    CONSTRAINT userph_pk PRIMARY KEY(USERNAME_ID)
) TABLESPACE INSTAGUSERS_ALL;

CREATE TABLE USERIMAGES_HISTORY
(   
    IMG_ID VARCHAR2(13), 
    XML_PATH VARCHAR2(62), 
    PHOTO_LINK VARCHAR2(70), 
    USERNAME_ID VARCHAR2(255), 
    ACTIVE NUMBER(1) DEFAULT 0,
    SCALABLE BLOB DEFAULT EMPTY_BLOB(),
    TAKEN_DATE DATE,
    CONSTRAINT userih_pk PRIMARY KEY(img_id)
) TABLESPACE INSTAGUSERS_ALL; 

DROP TABLE USERPROFILE;
CREATE TABLE USERPROFILE
(   
    USERNAME_ID VARCHAR2(255), 
    IMG_ID VARCHAR2(13),
    CONSTRAINT userp_pk PRIMARY KEY(USERNAME_ID)
) TABLESPACE INSTAGUSERS;

DROP TABLE USERIMAGES;
CREATE TABLE USERIMAGES
(   
    IMG_ID VARCHAR2(13), 
    USERNAME_ID VARCHAR2(255), 
    SUBTITLE VARCHAR2(30 BYTE), 
    TIME TIMESTAMP,
    SCALABLE BLOB DEFAULT EMPTY_BLOB(),
    CONSTRAINT useri_pk PRIMARY KEY(img_id),
    CONSTRAINT useri_user_fk FOREIGN KEY(USERNAME_ID) REFERENCES USERPROFILE(USERNAME_ID) ON  DELETE CASCADE
) TABLESPACE INSTAGUSERS; 

DROP INDEX UIMG_SCALABLE_EC2;
CREATE INDEX UIMG_SCALABLE_EC2 ON USERIMAGES(SCALABLE) INDEXTYPE IS SLIM_EUCLIDEAN PARAMETERS('8192');

DROP TABLE COMMENTS;
CREATE TABLE COMMENTS 
(   
    IMG_ID VARCHAR2(13), 
    USERNAME_ID VARCHAR2(255), 
    TEXT VARCHAR2(30), 
    TIME VARCHAR2(30), 
    COMMENT_ID NUMBER,
    CONSTRAINT comment_pk PRIMARY KEY(img_id,comment_id),
    CONSTRAINT comment_img_fk FOREIGN KEY(img_id) REFERENCES USERIMAGES(img_id) ON  DELETE CASCADE,
    CONSTRAINT comment_username FOREIGN KEY(USERNAME_ID) REFERENCES USERPROFILE(USERNAME_ID) ON  DELETE CASCADE
) TABLESPACE INSTAGENERAL;

DROP TABLE FOLLOWING;
CREATE TABLE FOLLOWING 
(   
    U_FOLLOWING VARCHAR2(255), 
    U_FOLLOWER VARCHAR2(255),
    CONSTRAINT following_pk PRIMARY KEY(u_following,u_follower),
    CONSTRAINT following_un UNIQUE(u_follower,u_following),
    CONSTRAINT following_fk_fo FOREIGN KEY(u_following) REFERENCES USERPROFILE(USERNAME_ID) ON  DELETE CASCADE,
    CONSTRAINT following_neq CHECK(u_following <> u_follower),
    CONSTRAINT following_fk_fl FOREIGN KEY(u_follower) REFERENCES USERPROFILE(USERNAME_ID) ON  DELETE CASCADE
) TABLESPACE INSTAGENERAL;

DROP TABLE LIKEDCOMMENTS;
CREATE TABLE LIKEDCOMMENTS 
(   
    USERNAME_ID VARCHAR2(255), 
    IMG_ID VARCHAR2(13), 
    COMMENT_ID NUMBER,
    CONSTRAINT likedc_pk PRIMARY KEY(img_id,USERNAME_ID,comment_id),
    CONSTRAINT likedc_user_fk FOREIGN KEY(USERNAME_ID) REFERENCES USERPROFILE(USERNAME_ID) ON  DELETE CASCADE,
    CONSTRAINT likedc_co_fk FOREIGN KEY(img_id,comment_id) REFERENCES COMMENTS(img_id,comment_id) ON  DELETE CASCADE
) TABLESPACE INSTAGENERAL;

DROP TABLE LIKEDIMAGES;
CREATE TABLE LIKEDIMAGES
(   
    USERNAME_ID VARCHAR2(255), 
    IMG_ID VARCHAR2(13),
    CONSTRAINT likedi_pk PRIMARY KEY(img_id,USERNAME_ID),
    CONSTRAINT likedi_fk FOREIGN KEY(USERNAME_ID) REFERENCES USERPROFILE(USERNAME_ID) ON  DELETE CASCADE,
    CONSTRAINT likedi_img_fk FOREIGN KEY(img_id) REFERENCES USERIMAGES(img_id) ON  DELETE CASCADE  
) TABLESPACE INSTAGENERAL;

CREATE TABLE SLIM_STATISTICS(
    descriptor VARCHAR2(15),
    img_id VARCHAR2(13),
    radius NUMBER,
    CREATE_DATE TIMESTAMP
);

CREATE SEQUENCE comment_id start with 1 increment by 1;

CREATE TABLE imagestags_history(
    img_id VARCHAR2(13), 
    tag VARCHAR2(100),
    constraint userit_pk primary key (img_id,tag),
    CONSTRAINT userit_img_fk FOREIGN KEY(img_id) REFERENCES userimages_history(img_id) ON DELETE CASCADE
); 

create or replace TRIGGER create_userimages
AFTER INSERT OR UPDATE ON userimages_history
FOR EACH ROW 
declare
     duplicate_info EXCEPTION;
     PRAGMA EXCEPTION_INIT (duplicate_info, -00001);
begin
    if :new.active = 1 then
        insert into userimages(img_id,username_id,scalable,time) values (:new.img_id,:new.username_id,:new.scalable,:new.taken_date);
    else
        delete from userimages where img_id = :new.img_id;
    end if;
EXCEPTION
    WHEN duplicate_info THEN
       RAISE_APPLICATION_ERROR (
         num=> -20107,
         msg=> 'Duplicate entry');     
end create_userimages;

create or replace TRIGGER create_userprofile
AFTER INSERT OR UPDATE ON userprofile_history
FOR EACH ROW 
declare
     duplicate_info EXCEPTION;
     PRAGMA EXCEPTION_INIT (duplicate_info, -00001);
begin
    if :new.active = 1 then
        insert into userprofile(img_id,username_id) values (:new.img_id,:new.username_id);
    else
        delete from userprofile where username_id = :new.username_id;
    end if;
EXCEPTION
    WHEN duplicate_info THEN
       RAISE_APPLICATION_ERROR (
         num=> -20107,
         msg=> 'Duplicate entry');     
end create_userprofile;

create or replace TRIGGER create_userprofile_history
BEFORE INSERT ON userimages_history
FOR EACH ROW 
declare
     username_v number;
     duplicate_info EXCEPTION;
     PRAGMA EXCEPTION_INIT (duplicate_info, -00001);
begin
    select count(1) into username_v from userprofile_history where username_id = :new.username_id;
    if username_v = 0 then
        insert into userprofile_history(username_id,img_id,active) values(:new.username_id,:new.img_id,:new.active);
    end if;
EXCEPTION
    WHEN duplicate_info THEN
       RAISE_APPLICATION_ERROR (
         num=> -20107,
         msg=> 'Duplicate entry');     
end create_userprofile_history;